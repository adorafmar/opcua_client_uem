var port = require('../config.js').get(process.env.NODE_ENV).port;
var fs = require('fs');
var express = require('express');
var app = express(); 
var nocache = require('nocache');
var http = require('http');
var https = require('https');
var logic = require("./logic");

app.use(nocache());
app.set('port', port);

app.get('/', function (req, res) {
	res.header('Access-Control-Allow-Origin' , '*');
	var node = req.query.node;
	res.send('parameter: '+node+'\n');
	logic.outer(node);
});

var server = app.listen(app.get('port'), function() {
  var port = server.address().port;
  console.log('http happens on port ' + port);
});

// var ports = 4443
// https.createServer({
//   key:  fs.readFileSync('id_rsa'),
//   cert: fs.readFileSync('outagemap_utilities_utexas_edu_cert.cer')
// }, app)
// .listen(ports, function () {
//   console.log('https happens on port %d', ports)
// });
