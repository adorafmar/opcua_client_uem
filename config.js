var config = {
  development: {
    ignition_machine: '10.101.206.141', // development
    port: 8080
  },
  default: {
    // ignition_machine: '10.101.206.15', // production
    // ignition_machine: '10.101.206.143', // staging
    ignition_machine: '127.0.0.1', // staging
    port: 8084
  }
}
exports.get = function get(env) {
  return config[env] || config.default;
}
